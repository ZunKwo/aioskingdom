﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class EscapeMenuScript : MonoBehaviour
{
    public Transform escapeObject;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            escapeObject.gameObject.SetActive(!escapeObject.gameObject.activeSelf);
            GlobalKeeperScript.Instance.IsPlayerInputEnabled = !escapeObject.gameObject.activeSelf;
        }
    }

    public void Resume()
    {
        escapeObject.gameObject.SetActive(!escapeObject.gameObject.activeSelf);
        GlobalKeeperScript.Instance.IsPlayerInputEnabled = !escapeObject.gameObject.activeSelf;
    }

    public void Back()
    {
        escapeObject.gameObject.SetActive(!escapeObject.gameObject.activeSelf);
        SceneManager.LoadScene("WeaponChoice");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
