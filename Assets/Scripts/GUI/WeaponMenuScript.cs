﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class WeaponMenuScript : MonoBehaviour
{
    [System.Serializable]
    public struct WeaponButton
    {
        public Transform Prefab;
        public Button Button;
    }

    [Header("Weapons")]
    public Transform weaponList;
    public WeaponButton[] weapons = new WeaponButton[1];
    public bool weaponSelected = false;
    private int selectedIndex = -1;

    [Header("Weapon Infos")]
    public Transform weaponInfos;
    public Transform weaponTrees;
    public Text weaponName;
    public Text weaponType;
    public Text weaponAttackSpeed;
    public Text weaponMinDamages;
    public Text weaponMaxDamages;

    [Header("Weapon Skill")]
    public Transform skillButtonPrefab;
    public Transform skillSelectionPrefab;
    public Transform skillView;
    public Transform skillList;
    public Transform skillSelection;
    private int treeIndex = -1;

    public Transform skillInfos;
    public Text skillTitle;
    public Text skillCooldown;
    public Text skillCost;
    public Text skillTooltip;
    private int talentIndex = -1;
    private int skillIndex = -1;

    void Start()
    {
        for (int i = 0; i < weapons.Length; ++i)
        {
            weapons[i].Prefab = Instantiate(weapons[i].Prefab) as Transform;
            weapons[i].Prefab.gameObject.SetActive(false);

            weapons[i].Button.GetComponentInChildren<Text>().text = weapons[i].Prefab.GetComponent<WeaponScript>().weaponStats.name;
        }

        Vector2 size = weaponList.GetComponent<RectTransform>().sizeDelta;
        weaponList.GetComponent<RectTransform>().sizeDelta = new Vector2(size.x, (weapons.Length * 100));
        weaponList.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);

        skillInfos.gameObject.SetActive(false);
        skillView.gameObject.SetActive(false);
        weaponInfos.gameObject.SetActive(false);
        weaponTrees.gameObject.SetActive(false);
    }

    public void SetPlayerWeapon(string index)
    {
        int idx = int.Parse(index);

        if (selectedIndex >= 0)
            weapons[selectedIndex].Button.image.color = Color.white;
        selectedIndex = idx;
        weapons[selectedIndex].Button.image.color = Color.blue;

        weapons[selectedIndex].Prefab.SetParent(GlobalKeeperScript.Instance.transform, false);
        GlobalKeeperScript.Instance.DataTransform["Weapon"] = weapons[selectedIndex].Prefab;
        weaponSelected = true;
        weaponInfos.gameObject.SetActive(true);
        weaponTrees.gameObject.SetActive(true);

        weaponName.text = weapons[selectedIndex].Prefab.GetComponent<WeaponScript>().weaponStats.name;
        weaponType.text = weapons[selectedIndex].Prefab.GetComponent<WeaponScript>().weaponStats.type.ToString();
        weaponAttackSpeed.text = weapons[selectedIndex].Prefab.GetComponent<WeaponScript>().weaponStats.attackRate.ToString();
        weaponMinDamages.text = weapons[selectedIndex].Prefab.GetComponent<WeaponScript>().weaponStats.minDamages.ToString();
        weaponMaxDamages.text = weapons[selectedIndex].Prefab.GetComponent<WeaponScript>().weaponStats.maxDamages.ToString();

        var children = new List<GameObject>();
        foreach (Transform child in skillSelection) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));

        foreach (WeaponSkillsScript.Map binding in weapons[selectedIndex].Prefab.GetComponent<WeaponSkillsScript>().skillBinding)
        {
            SkillScript skll = weapons[selectedIndex].Prefab.GetComponent<WeaponSkillsScript>().skills[binding.index[0]].skills[binding.index[1]];
            var butt = Instantiate(skillSelectionPrefab) as Transform;
            butt.GetComponentInChildren<Text>().text = skll.Name;
            butt.SetParent(skillSelection, false);
        }
    }

    public void SelectWeaponTree(string index)
    {
        int idx = int.Parse(index);

        if (treeIndex >= 0)
            weaponTrees.GetChild(treeIndex).GetComponent<Button>().image.color = Color.white;
        treeIndex = idx;
        weaponTrees.GetChild(treeIndex).GetComponent<Button>().image.color = Color.blue;

        skillView.gameObject.SetActive(true);
        skillInfos.gameObject.SetActive(true);

        var children = new List<GameObject>();
        foreach (Transform child in skillList) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));

        int i = 0;
        foreach (SkillScript skill in weapons[selectedIndex].Prefab.GetComponent<WeaponSkillsScript>().skills[treeIndex].skills)
        {
            var butt = Instantiate(skillButtonPrefab) as Transform;
            butt.GetComponentInChildren<Text>().text = skill.Name;
            butt.SetParent(skillList, false);
            butt.GetComponent<SkillInfoMenuScript>().menuScript = this;
            butt.GetComponent<SkillInfoMenuScript>().skill = skill;
            butt.GetComponent<SkillInfoMenuScript>().talentIndex = treeIndex;
            butt.GetComponent<SkillInfoMenuScript>().skillIndex = i;
            ++i;
        }

        if (i > 0)
            SetSkillInfos(treeIndex, 0, weapons[selectedIndex].Prefab.GetComponent<WeaponSkillsScript>().skills[treeIndex].skills[0]);
        else
            ResetSkillInfos();

        Vector2 size = skillList.GetComponent<RectTransform>().sizeDelta;
        skillList.GetComponent<RectTransform>().sizeDelta = new Vector2(size.x, (i * 100));
        skillList.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
    }

    private void ResetSkillInfos()
    {
        talentIndex = -1;
        skillIndex = -1;

        skillTitle.text = "";
        skillCooldown.text = "";
        skillCost.text = "";
        skillTooltip.text = "";

        skillView.gameObject.SetActive(false);
        skillInfos.gameObject.SetActive(false);
    }

    public void SetSkillInfos(int talent, int index, SkillScript skill)
    {
        talentIndex = talent;
        skillIndex = index;

        skillTitle.text = skill.Name;
        skillCooldown.text = skill.Cooldown.ToString("F2");
        skillCost.text = skill.Cost.ToString();
        skillTooltip.text = skill.Tooltip();

        skillView.gameObject.SetActive(true);
        skillInfos.gameObject.SetActive(true);

        Transform skillBindingChoice = skillInfos.Find("SkillBinding");
        int i = 0;
        foreach (WeaponSkillsScript.Map binding in weapons[selectedIndex].Prefab.GetComponent<WeaponSkillsScript>().skillBinding)
        {
            if (binding.index[0] == talent && binding.index[1] == index)
                skillBindingChoice.GetChild(i).gameObject.GetComponent<Button>().image.color = Color.red;
            else
                skillBindingChoice.GetChild(i).gameObject.GetComponent<Button>().image.color = Color.white;
            ++i;
        }
    }

    public void BindSelectedSkill(string index)
    {
        int idx = int.Parse(index);

        if (talentIndex == -1 && skillIndex == -1)
            return;

        weapons[selectedIndex].Prefab.GetComponent<WeaponSkillsScript>().skillBinding[idx].index[0] = talentIndex;
        weapons[selectedIndex].Prefab.GetComponent<WeaponSkillsScript>().skillBinding[idx].index[1] = skillIndex;

        var children = new List<GameObject>();
        foreach (Transform child in skillSelection) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));

        foreach (WeaponSkillsScript.Map binding in weapons[selectedIndex].Prefab.GetComponent<WeaponSkillsScript>().skillBinding)
        {
            SkillScript skll = weapons[selectedIndex].Prefab.GetComponent<WeaponSkillsScript>().skills[binding.index[0]].skills[binding.index[1]];
            var butt = Instantiate(skillSelectionPrefab) as Transform;
            butt.GetComponentInChildren<Text>().text = skll.Name;
            butt.SetParent(skillSelection, false);
        }

        Transform skillBindingChoice = skillInfos.Find("SkillBinding");
        skillBindingChoice.GetChild(idx).gameObject.GetComponent<Button>().image.color = Color.red;
    }

    public void StartGame()
    {
        if (weaponSelected)
        {
            SceneManager.LoadScene("TestLevel");
        }
    }

}
