﻿using UnityEngine;
using System.Collections;

public class SkillInfoMenuScript : MonoBehaviour {

    public WeaponMenuScript menuScript;
    public SkillScript skill;
    public int talentIndex;
    public int skillIndex;

    public void SetSkillInfos()
    {
        menuScript.SetSkillInfos(talentIndex, skillIndex, skill);
    }
	
}
