﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HitGUIScript : MonoBehaviour
{

    public float duration;
    public uint damages;
    public float speed;

    private float currentDuration;
    private float xRandOri;

    void Awake()
    {
        currentDuration = 0.0f;
        xRandOri = Random.Range(-0.5f, 0.5f);
    }

    void Update()
    {
        transform.Find("Damages").GetComponent<Text>().text = damages.ToString();
        if (currentDuration < duration)
        {
            currentDuration += Time.deltaTime;
            transform.position = new Vector3(transform.position.x + (xRandOri * Time.deltaTime), transform.position.y + (Time.deltaTime * speed), transform.position.z);
        }
        else
        {
            Destroy(gameObject);
        }

    }
}
