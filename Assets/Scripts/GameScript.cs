﻿using UnityEngine;
using System.Collections;

public class GameScript : MonoBehaviour
{

    public Transform playerPrefab;

    void Awake()
    {
        var player = Instantiate(playerPrefab) as Transform;
        player.GetComponent<PlayerScript>().SetWeapon(GlobalKeeperScript.Instance.DataTransform["Weapon"]);
    }

}
