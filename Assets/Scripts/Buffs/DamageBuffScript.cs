﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DamageBuffScript : BuffScript
{

    public int value;

    private bool apply = false;

    private uint minApplied;
    private uint maxApplied;

    void Update()
    {
        if (!started)
            return;

        if (!apply)
        {
            if (value > 0)
            {
                minApplied = (uint)value;
                target.GetComponentInChildren<WeaponScript>().weaponStats.minDamages += minApplied;
            }
            else if (value < 0)
            {
                minApplied = (uint)(value * -1);

                if (target.GetComponentInChildren<WeaponScript>().weaponStats.minDamages < minApplied)
                    minApplied = target.GetComponentInChildren<WeaponScript>().weaponStats.minDamages;

                target.GetComponentInChildren<WeaponScript>().weaponStats.minDamages -= minApplied;
            }

            if (value > 0)
            {
                maxApplied = (uint)value;
                target.GetComponentInChildren<WeaponScript>().weaponStats.maxDamages += maxApplied;
            }
            else if (value < 0)
            {
                maxApplied = (uint)(value * -1);

                if (target.GetComponentInChildren<WeaponScript>().weaponStats.maxDamages < maxApplied)
                    maxApplied = target.GetComponentInChildren<WeaponScript>().weaponStats.maxDamages;

                target.GetComponentInChildren<WeaponScript>().weaponStats.maxDamages -= maxApplied;
            }

            apply = true;
        }

        transform.Find("Cooldown").GetComponent<Text>().text = (duration - currentDuration).ToString("F2");
        if (currentDuration < duration)
        {
            currentDuration += Time.deltaTime;
        }
        else
        {
            if (value > 0)
                target.GetComponentInChildren<WeaponScript>().weaponStats.minDamages -= minApplied;
            else if (value < 0)
                target.GetComponentInChildren<WeaponScript>().weaponStats.minDamages += minApplied;

            if (value > 0)
                target.GetComponentInChildren<WeaponScript>().weaponStats.maxDamages -= maxApplied;
            else if (value < 0)
                target.GetComponentInChildren<WeaponScript>().weaponStats.maxDamages += maxApplied;

            Destroy(gameObject);
        }
    }
}
