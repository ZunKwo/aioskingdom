﻿using UnityEngine;
using System.Collections;

public class BuffGUIManagerScript : MonoBehaviour
{
    public void AddBuff(Transform buff)
    {
        buff.GetComponent<BuffScript>().started = true;
        buff.SetParent(transform, false);
    }
}
