﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OTBuffScript : BuffScript
{
    public float tickDelta;

    public int valuePerTick;

    private float currentTickDelta = 0.0f;

    void Update()
    {
        if (!started)
            return;

        transform.Find("Cooldown").GetComponent<Text>().text = (duration - currentDuration).ToString("F2");
        if (currentDuration < duration)
        {
            currentDuration += Time.deltaTime;
            currentTickDelta += Time.deltaTime;
            if (currentTickDelta >= tickDelta)
            {
                currentTickDelta = currentTickDelta - tickDelta;
                if (valuePerTick > 0)
                {
                    if (target.GetComponent<PlayerScript>())
                        target.GetComponent<PlayerScript>().GetDamages((uint)valuePerTick);
                    else if (target.GetComponent<TargetScript>())
                        target.GetComponent<TargetScript>().GetDamages((uint)valuePerTick);
                }
                else if (valuePerTick < 0)
                    target.GetComponent<PlayerScript>().Heal((uint)(valuePerTick * -1));
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
