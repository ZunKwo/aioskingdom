﻿using UnityEngine;
using System.Collections;

public class HitGUIManagerScript : MonoBehaviour
{

    public Transform HitPrefab;

    public void AddHit(Vector3 position, uint value, float duration, float speed = 0.0f)
    {
        var buff = Instantiate(HitPrefab) as Transform;
        buff.SetParent(transform, false);
        buff.GetComponent<HitGUIScript>().damages = value;
        buff.GetComponent<HitGUIScript>().duration = duration;
        buff.GetComponent<HitGUIScript>().speed = speed;


        buff.transform.position = position;
    }
}
