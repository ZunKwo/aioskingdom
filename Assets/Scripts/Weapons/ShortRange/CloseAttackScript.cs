﻿using UnityEngine;
using System.Collections;

public class CloseAttackScript : MonoBehaviour
{

    public Transform player;

    public float speed = 1.0f;
    public float angle = 0.0f;
    public Vector3 axis = new Vector3(0, 0, 1);
    public uint damages = 0;
    public bool isAdvanced = false;
    public bool facingLeft = false;

    private Quaternion destination;

    private Animator anim;

    void Awake()
    {
        //anim = GetComponent<Animator>();
    }

    void Start()
    {
        if (facingLeft)
            transform.rotation = Quaternion.AngleAxis(angle / 2, axis) * transform.rotation;
        else
            transform.rotation = Quaternion.AngleAxis(angle / 2, -axis) * transform.rotation;
        destination = Quaternion.AngleAxis(angle, axis) * transform.rotation;
        transform.Find("Hitbox").GetComponent<CloseAttackCollisionScript>().player = player;
        transform.Find("Hitbox").GetComponent<CloseAttackCollisionScript>().damages = damages;
    }

    void Update()
    {
        // anim.SetBool("Advanced", isAdvanced);
        if (transform.rotation != destination)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, destination, (angle * speed) * Time.deltaTime);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
