﻿using UnityEngine;
using System.Collections;

public class SH_SkillTwoScript : SkillScript
{

    [System.Serializable]
    public class Stats
    {
        public float speed = 0.25f;
        public float angle = 30.0f;
        public int minDamages = 5;
        public int maxDamages = 10;
    }

    public Stats stats = new Stats();

    void Start()
    {
        WeaponScript weapon = GetComponent<WeaponScript>();
        Cooldown = weapon.weaponStats.attackRate;
        Current = 0;
    }

    void Update()
    {
        if (Current > 0)
            Current -= Time.deltaTime;
    }

    public override bool Action()
    {
        WeaponScript weapon = GetComponent<WeaponScript>();

        var attackTransform = Instantiate(skillPrefab) as Transform;
        attackTransform.SetParent(LayerManagerScript.Instance.GetLayer("Projectiles").transform);
        attackTransform.position = transform.position;

        Vector2 direction = Vector3.Normalize(weapon.weaponTargetPoint.position - transform.position);
        float rot_z = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        attackTransform.rotation = Quaternion.Euler(0f, 0f, rot_z - 180);

        CloseAttackScript attack = attackTransform.GetComponent<CloseAttackScript>();
        if (attack != null)
        {
            attack.player = transform.parent.transform;
            attack.speed = stats.speed;
            attack.angle = stats.angle;
            attack.isAdvanced = false;
            uint damages = (uint)(int)Random.Range(stats.minDamages, stats.maxDamages + 1);
            attack.damages = damages;
        }

        return true;
    }

    public override string Tooltip()
    {
        string Tooltip = "Attack over " + stats.angle.ToString("F2") + " angle at " + stats.speed.ToString() + " speed, and deals between " + (stats.minDamages).ToString()
            + " to " + (stats.maxDamages).ToString() + " damages.";
        return Tooltip;
    }
}
