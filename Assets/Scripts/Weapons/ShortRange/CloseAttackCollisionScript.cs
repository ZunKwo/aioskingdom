﻿using UnityEngine;
using System.Collections;

public class CloseAttackCollisionScript : MonoBehaviour
{

    public Transform player;

    public uint damages = 0;

    void OnTriggerEnter2D(Collider2D collider)
    {
        bool collided = false;

        if (collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            collided = true;
        }

        if (collider.gameObject.layer == LayerMask.NameToLayer("Player") || collider.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            if (collider.gameObject.GetInstanceID() != player.gameObject.GetInstanceID())
            {
                GameObject.Find("HitManager").GetComponent<HitGUIManagerScript>().AddHit(transform.position, damages, 1.0f, 0.5f);
                (collider.gameObject.GetComponent<PlayerScript>()).GetDamages(damages);
                /*player.GetComponent<PlayerStatsScript>().experience.Gain(40000);
                player.GetComponentInChildren<WeaponSkillsScript>().experience.Gain(40000);*/
                collided = true;
            }
        }

        if (collider.gameObject.layer == LayerMask.NameToLayer("Target"))
        {
            if (collider.gameObject.GetInstanceID() != player.gameObject.GetInstanceID())
            {
                GameObject.Find("HitManager").GetComponent<HitGUIManagerScript>().AddHit(transform.position, damages, 1.0f, 0.5f);
                (collider.gameObject.GetComponent<TargetScript>()).GetDamages(damages);
                /*player.GetComponent<PlayerStatsScript>().experience.Gain(40000);
                player.GetComponentInChildren<WeaponSkillsScript>().experience.Gain(40000);*/
                collided = true;
            }
        }
    }
}
