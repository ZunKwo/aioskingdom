﻿using UnityEngine;
using System.Collections;

public abstract class SkillScript : MonoBehaviour
{

    public string Name;
    public float Cooldown;
    public float Current;
    public float Cost;

    public Transform skillPrefab;

    public abstract bool Action();
    public abstract string Tooltip();
}
