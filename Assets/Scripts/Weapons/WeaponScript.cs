﻿using UnityEngine;
using System.Collections;

public class WeaponScript : MonoBehaviour
{
    public enum WeaponType
    {
        Bow = 0,
        Sword
    }

    static private string weaponGameObjectName = "Weapon";

    public int playerId = 0;

    private WeaponSkillsScript skills;

    [System.Serializable]
    public class WeaponStats
    {
        public string name;
        public WeaponType type;

        public uint cost = 5;
        public float attackRate = 0.25f;
        public float advancedCastingTime = 1.0f;

        public uint minDamages = 1;
        public uint maxDamages = 1;
        public float advancedDamagesRatio = 1.2f;
    }

    public Transform weaponOutputPoint;
    public Transform weaponTargetPoint;

    public bool canAttack = true;

    public float attackCooldown;
    public float currentAdvancedCastingTime;
    public bool isCastingAdvanced = false;

    public WeaponStats weaponStats = new WeaponStats();

    private Vector2 mousePos;

    void Awake()
    {
        gameObject.name = weaponGameObjectName;
        attackCooldown = 0.0f;
        currentAdvancedCastingTime = 0.0f;
    }

    void Update()
    {
        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos = new Vector2(mouseWorldPos.x, mouseWorldPos.y);

        Vector2 direction = Vector3.Normalize(mousePos - new Vector2(transform.position.x, transform.position.y));
        float rot_z = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z);
    }
}
