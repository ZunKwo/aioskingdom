﻿using UnityEngine;
using System.Collections;

public class WeaponSkillsScript : MonoBehaviour
{
    public enum SkillIndex
    {
        BasicAttackDown = 0,
        BasicAttackUp,
        SkillOne,
        SkillTwo,
        SkillThree,
        SkillFour
    }

    [System.Serializable]
    public struct Map
    {
        public int[] index;

        public Map(uint size)
        {
            index = new int[size];
        }
    }

    [System.Serializable]
    public struct TalentTree
    {
        public SkillScript[] skills;

        public TalentTree(uint size = 10)
        {
            skills = new SkillScript[size];
        }
    }


    public int PlayerId;

    [System.Serializable]
    public struct Experience
    {
        public uint Level;
        public uint LevelMax;
        public uint Base;
        public float LevelRatio;

        private uint current;

        public Experience(uint level, uint max, uint baseExp, float ratio)
        {
            Level = level;
            LevelMax = max;
            Base = baseExp;
            LevelRatio = ratio;
            current = 0;
        }

        public uint GetMax()
        {
            return (uint)(int)(Base * LevelRatio * Level);
        }

        public void Gain(uint value)
        {
            if (Level == LevelMax)
                return;

            current += value;

            if (current >= GetMax())
            {
                uint overflow = current - GetMax();
                current = 0;
                ++Level;

                if (overflow > 0)
                    Gain(overflow);
            }
        }

        public float Current()
        {
            return current;
        }

    }

    public Experience experience = new Experience(1, 50, 400, 1.2f);
    public Map[] skillBinding = new Map[4];

    public SkillScript[] basicSkills = new SkillScript[2];

    public TalentTree[] skills = new TalentTree[4];

    void Update()
    {
        WeaponScript weapon = GetComponent<WeaponScript>();

        if (weapon.isCastingAdvanced)
        {
            if (weapon.currentAdvancedCastingTime < weapon.weaponStats.advancedCastingTime)
                weapon.currentAdvancedCastingTime += Time.deltaTime;
        }

        if (weapon.attackCooldown > 0.0f)
        {
            weapon.attackCooldown -= Time.deltaTime;
        }
    }

    public void UseSkill(SkillIndex index)
    {
        WeaponScript weapon = GetComponent<WeaponScript>();
        PlayerStatsScript playerStats = GetComponentInParent<PlayerStatsScript>();

        switch (index)
        {
            case SkillIndex.BasicAttackDown:
                if (weapon.canAttack)
                {
                        weapon.isCastingAdvanced = true;
                }
                break;
            case SkillIndex.BasicAttackUp:
                if (weapon.canAttack)
                {
                    int skillId = 0;
                    if (weapon.currentAdvancedCastingTime >= weapon.weaponStats.advancedCastingTime)
                        skillId = 1;

                    if (basicSkills[skillId].Current <= 0)
                        if (playerStats.energy.Use(weapon.weaponStats.cost))
                            if (basicSkills[skillId].Action())
                                basicSkills[skillId].Current = basicSkills[skillId].Cooldown;
                }
                weapon.currentAdvancedCastingTime = 0.0f;
                weapon.isCastingAdvanced = false;
                break;
            case SkillIndex.SkillOne:
                if (skills[skillBinding[0].index[0]].skills[skillBinding[0].index[1]].Current <= 0)
                {
                    if (playerStats.mana.Use(skills[skillBinding[0].index[0]].skills[skillBinding[0].index[1]].Cost))
                    {
                        if (skills[skillBinding[0].index[0]].skills[skillBinding[0].index[1]].Action())
                            skills[skillBinding[0].index[0]].skills[skillBinding[0].index[1]].Current = skills[skillBinding[0].index[0]].skills[skillBinding[0].index[1]].Cooldown;
                    }
                }
                break;
            case SkillIndex.SkillTwo:
                if (skills[skillBinding[1].index[0]].skills[skillBinding[1].index[1]].Current <= 0)
                {
                    if (playerStats.mana.Use(skills[skillBinding[1].index[0]].skills[skillBinding[1].index[1]].Cost))
                    {
                        if (skills[skillBinding[1].index[0]].skills[skillBinding[1].index[1]].Action())
                            skills[skillBinding[1].index[0]].skills[skillBinding[1].index[1]].Current = skills[skillBinding[1].index[0]].skills[skillBinding[1].index[1]].Cooldown;
                    }
                }
                break;
            case SkillIndex.SkillThree:
                if (skills[skillBinding[2].index[0]].skills[skillBinding[2].index[1]].Current <= 0)
                {
                    if (playerStats.mana.Use(skills[skillBinding[2].index[0]].skills[skillBinding[2].index[1]].Cost))
                    {
                        if (skills[skillBinding[2].index[0]].skills[skillBinding[2].index[1]].Action())
                            skills[skillBinding[2].index[0]].skills[skillBinding[2].index[1]].Current = skills[skillBinding[0].index[0]].skills[skillBinding[0].index[1]].Cooldown;
                    }
                }
                break;
            case SkillIndex.SkillFour:
                if (skills[skillBinding[3].index[0]].skills[skillBinding[3].index[1]].Current <= 0)
                {
                    if (playerStats.mana.Use(skills[skillBinding[3].index[0]].skills[skillBinding[3].index[1]].Cost))
                    {
                        if (skills[skillBinding[3].index[0]].skills[skillBinding[3].index[1]].Action())
                            skills[skillBinding[3].index[0]].skills[skillBinding[3].index[1]].Current = skills[skillBinding[3].index[0]].skills[skillBinding[3].index[1]].Cooldown;
                    }
                }
                break;
        }
    }
}
