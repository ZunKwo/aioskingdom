﻿using UnityEngine;
using System.Collections;

public class LR_HealOverTimeBuffSkillScript : SkillScript
{

    [System.Serializable]
    public class Stats
    {
        public int healTickValue = 10;
        public float duration = 10.0f;
        public float tickDuration = 1.0f;
    }

    public Stats stats = new Stats();

    void Start()
    {
        Current = 0;
    }

    void Update()
    {
        if (Current > 0)
            Current -= Time.deltaTime;
    }

    public override bool Action()
    {
        var buffTransform = Instantiate(skillPrefab) as Transform;
        GameObject.Find("BuffManager").GetComponent<BuffGUIManagerScript>().AddBuff(buffTransform);

        OTBuffScript buff = buffTransform.GetComponent<OTBuffScript>();
        if (buff != null)
        {
            buff.target = transform.parent.transform;
            buff.valuePerTick = -stats.healTickValue;
            buff.duration = stats.duration;
            buff.tickDelta = stats.tickDuration;
        }

        return true;
    }

    public override string Tooltip()
    {
        string Tooltip = "Heal over " + stats.duration.ToString("F2") +" seconds, healing " + stats.healTickValue.ToString()  + " hp each " + stats.tickDuration.ToString("F2") + " seconds.";
        return Tooltip;
    }
}
