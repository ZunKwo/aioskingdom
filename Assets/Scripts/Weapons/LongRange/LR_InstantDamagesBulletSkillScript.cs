﻿using UnityEngine;
using System.Collections;

public class LR_InstantDamagesBulletSkillScript : SkillScript
{

    [System.Serializable]
    public class Stats
    {
        public float bulletSpeed = 5.0f;
        public int minDamages = 5;
        public int maxDamages = 10;
    }

    public Stats stats = new Stats();

    void Start()
    {
        Current = 0;
    }

    void Update()
    {
        if (Current > 0)
            Current -= Time.deltaTime;
    }

    public override bool Action()
    {
        WeaponScript weapon = GetComponent<WeaponScript>();

        var projectileTransform = Instantiate(skillPrefab) as Transform;
        projectileTransform.SetParent(LayerManagerScript.Instance.GetLayer("Projectiles").transform);
        projectileTransform.position = weapon.weaponOutputPoint.position;

        BulletScript bullet = projectileTransform.GetComponent<BulletScript>();
        if (bullet != null)
        {
            bullet.player = transform.parent.transform;
            bullet.speed = stats.bulletSpeed;
            bullet.direction = Vector3.Normalize(weapon.weaponTargetPoint.position - transform.position);
            bullet.isAdvanced = false;
            uint damages = (uint)(int)Random.Range(stats.minDamages, stats.maxDamages + 1);
            bullet.damages = damages;

            float rot_z = Mathf.Atan2(bullet.direction.y, bullet.direction.x) * Mathf.Rad2Deg;
            bullet.transform.rotation = Quaternion.Euler(0f, 0f, rot_z);
        }

        return true;
    }

    public override string Tooltip()
    {
        string Tooltip = "Throw a bullet at " + stats.bulletSpeed.ToString() + " speed, and deals between " + (stats.minDamages).ToString()
            + " to " + (stats.maxDamages).ToString() + " damages.";
        return Tooltip;
    }
}
