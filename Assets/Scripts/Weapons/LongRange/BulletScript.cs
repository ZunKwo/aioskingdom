﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletScript : MonoBehaviour
{

    public Transform player;

    public float speed = 20;
    public Vector2 direction;
    public uint damages = 0;
    public bool isAdvanced = false;

    public Transform explosionPrefab;
    public List<Transform> effects = new List<Transform>();

    private Vector2 movement;
    private Rigidbody2D body;

    private Animator anim;

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        movement = new Vector2(speed * direction.x, speed * direction.y);
        anim.SetBool("Advanced", isAdvanced);
    }

    void FixedUpdate()
    {
        body.velocity = movement;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        bool collided = false;

        if (collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            collided = true;
        }

        if (collider.gameObject.layer == LayerMask.NameToLayer("Player")
            || collider.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            if (collider.gameObject.GetInstanceID() != player.gameObject.GetInstanceID())
            {
                if (effects.Count > 0)
                {
                    foreach (Transform effect in effects)
                    {
                        GameObject.Find("BuffManager").GetComponent<BuffGUIManagerScript>().AddBuff(effect);
                        effect.GetComponent<BuffScript>().target = collider.gameObject.transform;
                    }
                    effects.Clear();
                }

                damages = (collider.gameObject.GetComponent<PlayerScript>()).GetDamages(damages);
                if (damages > 0)
                {
                    GameObject.Find("HitManager").GetComponent<HitGUIManagerScript>().AddHit(transform.position, damages, 1.0f, 0.5f);
                }
                /*player.GetComponent<PlayerStatsScript>().experience.Gain(damages);
                player.GetComponentInChildren<WeaponSkillsScript>().experience.Gain(damages);*/
                collided = true;
            }
        }

        if (collider.gameObject.layer == LayerMask.NameToLayer("Target"))
        {
            if (collider.gameObject.GetInstanceID() != player.gameObject.GetInstanceID())
            {
                if (effects.Count > 0)
                {
                    foreach (Transform effect in effects)
                    {
                        (collider.gameObject.GetComponent<TargetGUIScript>()).GetBuffManager().GetComponent<BuffGUIManagerScript>().AddBuff(effect);
                        effect.GetComponent<BuffScript>().target = collider.gameObject.transform;
                    }
                    effects.Clear();
                }

                if (damages > 0)
                {
                    (collider.gameObject.GetComponent<TargetScript>()).GetDamages(damages);
                    GameObject.Find("HitManager").GetComponent<HitGUIManagerScript>().AddHit(transform.position, damages, 1.0f, 0.5f);
                }
                /*player.GetComponent<PlayerStatsScript>().experience.Gain(40000);
                player.GetComponentInChildren<WeaponSkillsScript>().experience.Gain(40000);*/
                collided = true;
            }
        }

        if (collided)
        {
            var explosionTransform = Instantiate(explosionPrefab) as Transform;
            explosionTransform.SetParent(LayerManagerScript.Instance.GetLayer("Projectiles"));
            explosionTransform.position = transform.position;

            if (effects.Count > 0)
            {
                foreach (Transform effect in effects)
                {
                    Destroy(effect.gameObject);
                }
                effects.Clear();
            }

            Destroy(gameObject);
        }
    }
}
