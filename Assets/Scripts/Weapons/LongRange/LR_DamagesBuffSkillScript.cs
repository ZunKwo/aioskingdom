﻿using UnityEngine;
using System.Collections;

public class LR_DamagesBuffSkillScript : SkillScript
{

    [System.Serializable]
    public class Stats
    {
        public int buffValue = 10;
        public float duration = 1.0f;
    }

    public Stats stats = new Stats();

    void Start()
    {
        Current = 0;
    }

    void Update()
    {
        if (Current > 0)
            Current -= Time.deltaTime;
    }

    public override bool Action()
    {
        var buffTransform = Instantiate(skillPrefab) as Transform;
        GameObject.Find("BuffManager").GetComponent<BuffGUIManagerScript>().AddBuff(buffTransform);

        DamageBuffScript buff = buffTransform.GetComponent<DamageBuffScript>();
        if (buff != null)
        {
            buff.target = transform.parent.transform;
            buff.value = stats.buffValue;
            buff.duration = stats.duration;
        }

        return true;
    }

    public override string Tooltip()
    {
        string Tooltip = "Buff your damages for " + stats.buffValue + " damages over " + stats.duration.ToString("F2")
            + " seconds";
        return Tooltip;
    }
}
