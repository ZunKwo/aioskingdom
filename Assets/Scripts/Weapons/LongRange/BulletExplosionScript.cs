﻿using UnityEngine;
using System.Collections;

public class BulletExplosionScript : MonoBehaviour {

    private float length;

    void Start()
    {
        length = GetComponent<Animator>().runtimeAnimatorController.animationClips[0].length;
    }

	void Update () {
        if (length > 0.0f)
            length -= Time.deltaTime;

        if (length <= 0.0f)
            Destroy(gameObject);
	}
}
