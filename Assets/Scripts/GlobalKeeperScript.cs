﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalKeeperScript : MonoBehaviour
{
    public Dictionary<string, string> DataString = new Dictionary<string,string>();
    public Dictionary<string, Transform> DataTransform = new Dictionary<string, Transform>();

    public bool IsPlayerInputEnabled = true;

    private static GlobalKeeperScript _instance = null;
    public static GlobalKeeperScript Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;

        DontDestroyOnLoad(gameObject);
    }
}
