﻿using UnityEngine;
using System.Collections;

public class TargetScript : MonoBehaviour
{

    public uint health = 10;

    private uint current;

    void Start()
    {
        current = health;
    }

    public void GetDamages(uint value)
    {
        if (value < current)
            current -= value;
        else
            current = 0;

        if (current <= 0)
        {
            gameObject.SetActive(false);
            GetComponent<TargetGUIScript>().Disable();
        }
    }

    public uint getCurrent()
    {
        return current;
    }
}
