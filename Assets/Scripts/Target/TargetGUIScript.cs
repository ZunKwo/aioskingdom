﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TargetGUIScript : MonoBehaviour
{

    public Transform guiprefab;

    private Slider healthSlider;
    private Text healthText;

    private TargetScript stats;
    private Transform guiLayer;
    private Transform playerGui;

    void Start()
    {
        guiLayer = LayerManagerScript.Instance.GetLayer("GUI");
        playerGui = Instantiate(guiprefab) as Transform;
        playerGui.SetParent(guiLayer.transform, false);

        stats = gameObject.GetComponent<TargetScript>();

        healthSlider = playerGui.transform.Find("HealthUI").transform.Find("HealthBar").GetComponent<Slider>();
        healthSlider.minValue = 0;
        healthSlider.maxValue = stats.health;
        healthSlider.value = stats.getCurrent();
        healthText = playerGui.transform.Find("HealthUI").transform.Find("HealthValue").GetComponent<Text>();
        healthText.text = stats.getCurrent().ToString() + " / " + stats.health.ToString();
    }

    void Update()
    {
        healthSlider.value = stats.getCurrent();
        healthText.text = stats.getCurrent().ToString() + " / " + stats.health.ToString();
        playerGui.transform.position = new Vector3(transform.position.x, transform.position.y, 0.0f) + new Vector3(0, (GetComponent<BoxCollider2D>().size.y / 2) + 1.0f, 0.0f);
    }

    public void Disable()
    {
        playerGui.gameObject.SetActive(false);
    }

    public Transform GetBuffManager()
    {
        return playerGui.transform.Find("BuffGUI");
    }
}
