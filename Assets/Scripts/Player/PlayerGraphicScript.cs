﻿using UnityEngine;
using System.Collections;

public class PlayerGraphicScript : MonoBehaviour
{
    private Transform effect;

    private bool faceLeft = true;
    private Vector2 mousePos;

    private Animator anim;
    private Transform shieldSprite;

    private PlayerScript movementScript;

    // Use this for initialization
    void Start()
    {
        effect = transform.Find("Effects").transform;
        anim = GetComponent<Animator>();
        shieldSprite = transform.Find("Shield").transform;
        movementScript = GetComponent<PlayerScript>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos = new Vector2(mouseWorldPos.x, mouseWorldPos.y);

        float inputX = Input.GetAxis("Horizontal");

        if (!faceLeft && (transform.position.x - mousePos.x) > 0.0f)
            Flip();
        else if (faceLeft && (transform.position.x - mousePos.x) < 0.0f)
            Flip();

        if (inputX < 0 && !faceLeft)
            anim.SetBool("Backward", true);
        else if (inputX > 0 && faceLeft)
            anim.SetBool("Backward", true);
        else
            anim.SetBool("Backward", false);

        shieldSprite.gameObject.SetActive(GetComponent<PlayerScript>().dodge.Active);

        anim.SetBool("Grounded", movementScript.IsGrounded());
        anim.SetFloat("Speed", movementScript.GetAbsVelocity());

        effect.GetComponent<Animator>().SetBool("Resplendish", movementScript.resplendish.Active);
    }

    void Flip()
    {
        faceLeft = !faceLeft;
        GetComponent<SpriteRenderer>().flipX = !faceLeft;
    }

}
