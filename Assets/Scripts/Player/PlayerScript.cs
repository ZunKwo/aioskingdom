﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour
{
    private PlayerStatsScript stats;

    public Transform groundCheck;
    public Transform rightWallCheck;
    public Transform leftWallCheck;

    public LayerMask whatIsGround;

    [System.Serializable]
    public struct Dash
    {
        public float Cooldown;
        public float Velocity;
        public int EnergyCost;
        public float Current;

        public Dash(float cd, float vel, int cost)
        {
            Cooldown = cd;
            Velocity = vel;
            EnergyCost = cost;
            Current = 0;
        }
    }
    public Dash dash = new Dash(2.0f, 200.0f, 10);

    [System.Serializable]
    public struct Dodge
    {
        public float Cooldown;
        public float Duration;
        public int EnergyCost;
        public float CurrentCooldown;
        public float CurrentDuration;
        public bool Active;

        public Dodge(float cd, float dur, int cost)
        {
            Cooldown = cd;
            Duration = dur;
            EnergyCost = cost;
            CurrentCooldown = 0;
            CurrentDuration = 0;
            Active = false;
        }
    }
    public Dodge dodge = new Dodge(10.0f, 2.0f, 30);

    [System.Serializable]
    public struct Resplendish
    {
        public float CastTime;
        public int EnergyTickValue;
        public float EnergyTickRate;
        public float CurrentCastTime;
        public float CurrentTick;
        public bool Active;

        public Resplendish(float cast, int tickVal, float tickRate)
        {
            CastTime = cast;
            EnergyTickValue = tickVal;
            EnergyTickRate = tickRate;
            CurrentCastTime = 0;
            CurrentTick = 0;
            Active = false;
        }
    }
    public Resplendish resplendish = new Resplendish(2.0f, 5, 0.2f);

    private Rigidbody2D body;

    bool grounded = false;
    float groundRadius = 0.2f;

    void Awake()
    {
        stats = GetComponent<PlayerStatsScript>();
        body = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
    }

    void Update()
    {
        if (dodge.CurrentDuration <= 0.0f)
            dodge.Active = false;

        if (dash.Current >= 0.0f)
            dash.Current -= Time.deltaTime;

        if (dodge.CurrentCooldown >= 0.0f)
            dodge.CurrentCooldown -= Time.deltaTime;

        if (dodge.CurrentDuration >= 0.0f)
            dodge.CurrentDuration -= Time.deltaTime;

        limitVelocityAction();
    }

    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
    }

    public void jumpAction()
    {
        if (grounded && !resplendish.Active)
        {
            body.AddForce(new Vector2(0, stats.speed.Jump));
        }
    }

    public void moveAction(float inputX)
    {

        if (inputX > 0.1)
            inputX = 1.0f;
        else if (inputX < -0.1)
            inputX = -1.0f;
        else
            inputX = 0;

        if (inputX > 0 && !resplendish.Active && !rightWallCheck.GetComponent<WallColliderScript>().isColliding && Mathf.Abs(body.velocity.x) < stats.speed.Max)
            body.AddForce(new Vector2(stats.speed.Movement * inputX, 0));
        else if (inputX < 0 && !resplendish.Active && !leftWallCheck.GetComponent<WallColliderScript>().isColliding && Mathf.Abs(body.velocity.x) < stats.speed.Max)
            body.AddForce(new Vector2(stats.speed.Movement * inputX, 0));
        else if (inputX == 0)
            body.AddForce(new Vector2(-body.velocity.x * stats.speed.Movement, 0));
    }

    public void dashAction(float inputX)
    {
        if (inputX != 0.0f && !resplendish.Active)
        {
            if (dash.Current <= 0.0f)
            {
                if (stats.energy.Use(dash.EnergyCost))
                {
                    float side;

                    if (inputX > 0)
                        side = 1;
                    else
                        side = -1;

                    body.AddForce(new Vector2(dash.Velocity * side, 0));
                    dash.Current = dash.Cooldown;
                }
            }
        }
    }

    public void shieldAction()
    {
        if (!resplendish.Active)
        {
            if (dodge.CurrentCooldown <= 0.0f && stats.energy.Use(dodge.EnergyCost))
            {
                dodge.Active = true;
                dodge.CurrentDuration = dodge.Duration;
                dodge.CurrentCooldown = dodge.Cooldown;

                //GameObject.Find("BuffManager").GetComponent<BuffGUIManagerScript>().AddBuff(transform, dodge.Duration, 0.0f);
            }
        }
    }

    public void resplendishAction(bool resplendishUp, bool resplendishDown)
    {
        if (resplendishDown)
            resplendish.Active = true;
        else if (resplendishUp)
        {
            resplendish.Active = false;
            resplendish.CurrentCastTime = 0.0f;
            GetComponentInChildren<WeaponScript>().canAttack = true;
        }

        if (resplendish.Active)
        {
            GetComponentInChildren<WeaponScript>().canAttack = false;
            if (resplendish.CurrentCastTime < resplendish.CastTime)
            {
                resplendish.CurrentCastTime += Time.deltaTime;
                resplendish.CurrentTick += Time.deltaTime;
                if (resplendish.CurrentTick >= resplendish.EnergyTickRate)
                {
                    resplendish.CurrentTick = resplendish.CurrentTick - resplendish.EnergyTickRate;
                    stats.energy.Gain(resplendish.EnergyTickValue);
                }
            }
            else
                resplendish.Active = false;
        }
    }

    void limitVelocityAction()
    {
        if (Mathf.Abs(body.velocity.x) > stats.speed.Max)
        {
            float newSpeed = stats.speed.Max - Mathf.Abs(body.velocity.x);
            if (body.velocity.x > 0.0f)
                newSpeed *= -1;
            body.AddForce(new Vector2(newSpeed, 0));
        }
    }

    public void SetWeapon(Transform weapon)
    {
        if (transform.Find("Weapon"))
            Destroy(transform.Find("Weapon").gameObject);

        weapon.gameObject.SetActive(true);
        weapon.SetParent(transform, false);
        weapon.GetComponent<WeaponScript>().playerId = gameObject.GetInstanceID();
    }

    public uint GetDamages(uint damages)
    {
        if (dodge.Active)
        {
            dodge.Active = false;
            return 0;
        }

        uint reduced = stats.physicArmor.Reduce(damages);

        stats.health.Current -= reduced;

        if (stats.health.Current < 0)
            stats.health.Current = 0;

        return reduced;
    }

    public void Heal(uint value)
    {
        stats.health.Current += value;

        if (stats.health.Current > stats.health.Max)
            stats.health.Current = stats.health.Max;
    }

    public float GetResplendishCooldown()
    {
        return resplendish.CurrentCastTime;
    }

    public float GetDashCooldown()
    {
        return dash.Current;
    }

    public float GetShieldCooldown()
    {
        return dodge.CurrentCooldown;
    }

    public bool IsGrounded()
    {
        return grounded;
    }

    public float GetAbsVelocity()
    {
        return Mathf.Abs(body.velocity.x);
    }
}
