﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerGUIScript : MonoBehaviour
{

    private Slider healthSlider;
    private Text healthText;

    private Slider energySlider;
    private Text energyText;

    private Slider manaSlider;
    private Text manaText;

    private Slider experienceSlider;
    private Text experienceText;

    private Slider RWexperienceSlider;
    private Text RWexperienceText;

    private Slider LWexperienceSlider;
    private Text LWexperienceText;

    private PlayerStatsScript stats;

    // Use this for initialization
    void Start()
    {
        PlayerTeamScript teamScript = GetComponent<PlayerTeamScript>();
        stats = GetComponent<PlayerStatsScript>();

        healthSlider = teamScript.GetPlayerGUI().transform.Find("HealthUI").transform.Find("HealthBar").GetComponent<Slider>();
        healthSlider.minValue = 0;
        healthSlider.maxValue = stats.health.Max;
        healthSlider.value = stats.health.Current;
        healthText = teamScript.GetPlayerGUI().transform.Find("HealthUI").transform.Find("HealthValue").GetComponent<Text>();
        healthText.text = stats.health.Current.ToString() + " / " + stats.health.Max.ToString();

        energySlider = teamScript.GetPlayerGUI().transform.Find("EnergyUI").transform.Find("EnergyBar").GetComponent<Slider>();
        energySlider.minValue = 0;
        energySlider.maxValue = stats.energy.Max;
        energySlider.value = stats.energy.Current;
        energyText = teamScript.GetPlayerGUI().transform.Find("EnergyUI").transform.Find("EnergyValue").GetComponent<Text>();
        energyText.text = stats.energy.Current.ToString() + " / " + stats.energy.Max.ToString();

        manaSlider = teamScript.GetPlayerGUI().transform.Find("ManaUI").transform.Find("ManaBar").GetComponent<Slider>();
        manaSlider.minValue = 0;
        manaSlider.maxValue = stats.mana.Max;
        manaSlider.value = stats.mana.Current;
        manaText = teamScript.GetPlayerGUI().transform.Find("ManaUI").transform.Find("ManaValue").GetComponent<Text>();
        manaText.text = stats.mana.Current.ToString() + " / " + stats.mana.Max.ToString();

        experienceSlider = teamScript.GetPlayerGUI().transform.Find("ExperienceUI").transform.Find("ExperienceBar").GetComponent<Slider>();
        experienceSlider.minValue = 0;
        experienceSlider.maxValue = stats.experience.GetMax();
        experienceSlider.value = stats.experience.Current();
        experienceText = teamScript.GetPlayerGUI().transform.Find("ExperienceUI").transform.Find("ExperienceValue").GetComponent<Text>();
        experienceText.text = stats.mana.Current.ToString() + " / " + stats.mana.Max.ToString();

        RWexperienceSlider = teamScript.GetPlayerGUI().transform.Find("RWExperienceUI").transform.Find("ExperienceBar").GetComponent<Slider>();
        RWexperienceSlider.minValue = 0;
        RWexperienceSlider.maxValue = stats.experience.GetMax();
        RWexperienceSlider.value = stats.experience.Current();
        RWexperienceText = teamScript.GetPlayerGUI().transform.Find("RWExperienceUI").transform.Find("ExperienceValue").GetComponent<Text>();
        RWexperienceText.text = stats.mana.Current.ToString() + " / " + stats.mana.Max.ToString();

        LWexperienceSlider = teamScript.GetPlayerGUI().transform.Find("LWExperienceUI").transform.Find("ExperienceBar").GetComponent<Slider>();
        LWexperienceSlider.minValue = 0;
        LWexperienceSlider.maxValue = stats.experience.GetMax();
        LWexperienceSlider.value = stats.experience.Current();
        LWexperienceText = teamScript.GetPlayerGUI().transform.Find("LWExperienceUI").transform.Find("ExperienceValue").GetComponent<Text>();
        LWexperienceText.text = stats.mana.Current.ToString() + " / " + stats.mana.Max.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerTeamScript teamScript = GetComponent<PlayerTeamScript>();
        PlayerScript advScript = GetComponent<PlayerScript>();
        Transform weapon = transform.Find("Weapon");
        WeaponSkillsScript skillScript = weapon.GetComponent<WeaponSkillsScript>();

        healthSlider.value = stats.health.Current;
        healthText.text = stats.health.Current.ToString() + " / " + stats.health.Max.ToString();

        energySlider.value = stats.energy.Current;
        energyText.text = stats.energy.Current.ToString() + " / " + stats.energy.Max.ToString();

        manaSlider.value = stats.mana.Current;
        manaText.text = stats.mana.Current.ToString() + " / " + stats.mana.Max.ToString();

        experienceSlider.value = stats.experience.Current();
        experienceSlider.maxValue = stats.experience.GetMax();
        experienceText.text = stats.experience.Current().ToString() + " / " + stats.experience.GetMax().ToString();

        RWexperienceSlider.value = skillScript.experience.Current();
        RWexperienceSlider.maxValue = skillScript.experience.GetMax();
        RWexperienceText.text = skillScript.experience.Current().ToString() + " / " + skillScript.experience.GetMax().ToString();

        LWexperienceSlider.value = skillScript.experience.Current();
        LWexperienceSlider.maxValue = skillScript.experience.GetMax();
        LWexperienceText.text = skillScript.experience.Current().ToString() + " / " + skillScript.experience.GetMax().ToString();

        teamScript.GetPlayerGUI().transform.Find("LevelUI").Find("Level").GetComponent<Text>().text = advScript.GetComponent<PlayerStatsScript>().experience.Level.ToString();
        teamScript.GetPlayerGUI().transform.Find("RWLevelUI").Find("Level").GetComponent<Text>().text = skillScript.experience.Level.ToString();
        teamScript.GetPlayerGUI().transform.Find("LWLevelUI").Find("Level").GetComponent<Text>().text = skillScript.experience.Level.ToString();

        teamScript.GetPlayerGUI().transform.Find("Resplendish").Find("Cooldown").GetComponent<Text>().text = (advScript.GetResplendishCooldown() > 0.0f ? advScript.GetResplendishCooldown().ToString("F2") : "");
        teamScript.GetPlayerGUI().transform.Find("Dash").Find("Cooldown").GetComponent<Text>().text = (advScript.GetDashCooldown() > 0.0f ? advScript.GetDashCooldown().ToString("F2") : "");
        teamScript.GetPlayerGUI().transform.Find("Shield").Find("Cooldown").GetComponent<Text>().text = (advScript.GetShieldCooldown() > 0.0f ? advScript.GetShieldCooldown().ToString("F2") : "");
        teamScript.GetPlayerGUI().transform.Find("Advanced").Find("Bar").GetComponent<Slider>().value = weapon.GetComponent<WeaponScript>().currentAdvancedCastingTime;
        teamScript.GetPlayerGUI().transform.Find("Advanced").Find("Bar").GetComponent<Slider>().maxValue = weapon.GetComponent<WeaponScript>().weaponStats.advancedCastingTime;

        teamScript.GetPlayerGUI().transform.Find("SkillOne").Find("Cooldown").GetComponent<Text>().text = (skillScript.skills[skillScript.skillBinding[0].index[0]].skills[skillScript.skillBinding[0].index[1]].Current > 0.0f ? skillScript.skills[skillScript.skillBinding[0].index[0]].skills[skillScript.skillBinding[0].index[1]].Current.ToString("F2") : "");
        teamScript.GetPlayerGUI().transform.Find("SkillTwo").Find("Cooldown").GetComponent<Text>().text = (skillScript.skills[skillScript.skillBinding[1].index[0]].skills[skillScript.skillBinding[1].index[1]].Current > 0.0f ? skillScript.skills[skillScript.skillBinding[1].index[0]].skills[skillScript.skillBinding[1].index[1]].Current.ToString("F2") : "");
        teamScript.GetPlayerGUI().transform.Find("SkillThree").Find("Cooldown").GetComponent<Text>().text = (skillScript.skills[skillScript.skillBinding[2].index[0]].skills[skillScript.skillBinding[2].index[1]].Current > 0.0f ? skillScript.skills[skillScript.skillBinding[2].index[0]].skills[skillScript.skillBinding[2].index[1]].Current.ToString("F2") : "");
        teamScript.GetPlayerGUI().transform.Find("SkillFour").Find("Cooldown").GetComponent<Text>().text = (skillScript.skills[skillScript.skillBinding[3].index[0]].skills[skillScript.skillBinding[3].index[1]].Current > 0.0f ? skillScript.skills[skillScript.skillBinding[3].index[0]].skills[skillScript.skillBinding[3].index[1]].Current.ToString("F2") : "");

        Vector3 camPos = teamScript.transform.position;
        camPos.z = Camera.main.transform.position.z;
        Camera.main.transform.position = camPos;
    }
}
