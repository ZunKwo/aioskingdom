﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerTeamScript : MonoBehaviour
{

    public int teamNumber = 0;
    public int playerNumber = 0;
    private Transform guiLayer;
    public Transform playerOverGuiPrefab;
    public Transform playerGuiPrefab;
    public bool isPlayer = false;

    public string playerName = "Player";

    private Transform playerGui;

    void Start()
    {
        guiLayer = LayerManagerScript.Instance.GetLayer("GUI");
        gameObject.tag = GetTeamName() + "Player" + playerNumber.ToString();
        transform.SetParent(GameObject.Find("Game/" + GetTeamName() + "/" + GetSpawnName()).transform, false);

        if (isPlayer)
        {
            playerGui = Instantiate(playerGuiPrefab) as Transform;
            playerGui.SetParent(guiLayer.transform, false);
        }
        else
        {
            playerGui = Instantiate(playerOverGuiPrefab) as Transform;
            playerGui.SetParent(guiLayer.transform, false);
            if (GetComponent<PlayerGUIScript>() == null)
            {
                playerGui.Find("EnergyUI").gameObject.SetActive(false);
            }
        }
    }

    void Update()
    {
        if (!isPlayer)
        {
            playerGui.transform.position = new Vector3(transform.position.x, transform.position.y, 0.0f) + new Vector3(0, (GetComponent<BoxCollider2D>().size.y / 2) + 1.0f, 0.0f);
        }
    }

    string GetTeamName()
    {
        return "Team" + teamNumber.ToString();
    }

    string GetSpawnName()
    {
        return "Spawn" + playerNumber.ToString();
    }

    public Transform GetPlayerGUI()
    {
        return playerGui;
    }
}
