﻿using UnityEngine;
using System.Collections;

public class PlayerInputController : MonoBehaviour
{
    void Start()
    {

    }

    void Update()
    {
        PlayerScript player = GetComponent<PlayerScript>();
        WeaponSkillsScript skills = GetComponentInChildren<WeaponSkillsScript>();

        if (!GlobalKeeperScript.Instance.IsPlayerInputEnabled)
            return;

        if (Input.GetButtonDown("Fire1"))
            skills.UseSkill(WeaponSkillsScript.SkillIndex.BasicAttackDown);

        if (Input.GetButtonUp("Fire1"))
            skills.UseSkill(WeaponSkillsScript.SkillIndex.BasicAttackUp);

        if (Input.GetButtonDown("SkillOne"))
            skills.UseSkill(WeaponSkillsScript.SkillIndex.SkillOne);

        if (Input.GetButtonDown("SkillTwo"))
            skills.UseSkill(WeaponSkillsScript.SkillIndex.SkillTwo);

        if (Input.GetButtonDown("SkillThree"))
            skills.UseSkill(WeaponSkillsScript.SkillIndex.SkillThree);

        if (Input.GetButtonDown("SkillFour"))
            skills.UseSkill(WeaponSkillsScript.SkillIndex.SkillFour);

        if (Input.GetButtonDown("Jump"))
            player.jumpAction();

        if (Input.GetButtonDown("Dash"))
            player.dashAction(Input.GetAxis("Horizontal"));

        if (Input.GetButtonDown("Dodge"))
            player.shieldAction();

        player.resplendishAction(Input.GetButtonUp("Resplendish"), Input.GetButtonDown("Resplendish"));

        player.moveAction(Input.GetAxis("Horizontal"));
    }
}
