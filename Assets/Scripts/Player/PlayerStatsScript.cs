﻿using UnityEngine;
using System.Collections;

public class PlayerStatsScript : MonoBehaviour
{

    [System.Serializable]
    public struct Experience
    {
        public uint Level;
        public uint LevelMax;
        public uint Base;
        public float LevelRatio;

        private uint current;

        public Experience(uint level, uint max, uint baseExp, float ratio)
        {
            Level = level;
            LevelMax = max;
            Base = baseExp;
            LevelRatio = ratio;
            current = 0;
        }

        public uint GetMax()
        {
            return (uint)(int)(Base * LevelRatio * Level);
        }

        public void Gain(uint value)
        {
            if (Level == LevelMax)
                return;

            current += value;

            if (current >= GetMax())
            {
                uint overflow = current - GetMax();
                current = 0;
                ++Level;

                if (overflow > 0)
                    Gain(overflow);
            }
        }

        public float Current()
        {
            return current;
        }

    }

    [System.Serializable]
    public struct Health
    {
        public float Max;
        public float Current;

        public Health(float max)
        {
            Max = max;
            Current = max;
        }
    }

    [System.Serializable]
    public struct Energy
    {
        public float Max;
        public float Current;

        public Energy(float max)
        {
            Max = max;
            Current = max;
        }

        public bool Use(float value)
        {
            if (Current - value >= 0)
            {
                Current -= value;
                return true;
            }

            return false;
        }

        public void Gain(float value)
        {
            if (value > 0)
            {
                Current += value;

                if (Current > Max)
                    Current = Max;
            }
        }
    }

    [System.Serializable]
    public struct Mana
    {
        public float Max;
        public float Current;

        public Mana(float max)
        {
            Max = max;
            Current = max;
        }

        public bool Use(float value)
        {
            if (Current - value >= 0)
            {
                Current -= value;
                return true;
            }

            return false;
        }

        public void Gain(float value)
        {
            if (value > 0)
            {
                Current += value;

                if (Current > Max)
                    Current = Max;
            }
        }
    }

    [System.Serializable]
    public struct PhysicArmor
    {
        public uint Value;
        public uint ReducingRatio;

        public PhysicArmor(uint value, uint ratio)
        {
            Value = value;
            ReducingRatio = ratio;
        }

        public uint Reduce(uint damages)
        {
            uint reduced = damages - (Value * ReducingRatio);
            return reduced;
        }
    }

    [System.Serializable]
    public struct MagicArmor
    {
        public uint Value;
        public uint ReducingRatio;

        public MagicArmor(uint value, uint ratio)
        {
            Value = value;
            ReducingRatio = ratio;
        }

        public uint Reduce(uint damages)
        {
            uint reduced = damages - (Value * ReducingRatio);
            return reduced;
        }
    }

    [System.Serializable]
    public struct Speed
    {
        public float Movement;
        public float Jump;
        public float Max;

        public Speed(float mov, float jump, float max)
        {
            Movement = mov;
            Jump = jump;
            Max = max;
        }
    }

    public Experience experience = new Experience(1, 10, 400, 1.2f);
    public Health health = new Health(10);
    public Energy energy = new Energy(100);
    public Mana mana = new Mana(100);
    public PhysicArmor physicArmor = new PhysicArmor(5, 1);
    public MagicArmor magicArmor = new MagicArmor(5, 1);
    public Speed speed = new Speed(20, 600, 5);
    public bool isDodging = false;
}
