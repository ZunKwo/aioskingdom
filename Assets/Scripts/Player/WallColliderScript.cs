﻿using UnityEngine;
using System.Collections;

public class WallColliderScript : MonoBehaviour
{

    public bool isColliding = false;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
            isColliding = true;
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
            isColliding = false;
    }
}
