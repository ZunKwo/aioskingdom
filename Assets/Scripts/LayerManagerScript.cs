﻿using UnityEngine;
using System.Collections;

public class LayerManagerScript : MonoBehaviour
{
    [System.Serializable]
    public struct Layer
    {
        public string Name;
        public Transform LayerTransform;
    }

    public Layer[] layers = new Layer[1];


    private static LayerManagerScript _instance = null;
    public static LayerManagerScript Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
    }

    public Transform GetLayer(string name)
    {
        for (int i = 0; i < layers.Length; ++i)
        {
            if (layers[i].Name == name)
                return layers[i].LayerTransform;
        }

        return null;
    }
}
